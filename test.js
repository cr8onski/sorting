// #!/usr/bin/env node

/**
 * So what I want to do is:
 *   write sorts
 *   use those sorts
 *      on the same data
 *      vary increase the size of the data
 *   time and compare the sorts
 *   to see bigO notation at work
 */
console.time('Sorting');
console.log(new Date().toLocaleString());
const mainStartTime = Date.now();

// console.log(`\nWelcome to ${process.title}    ${process.uptime()}\n`);

// Config Section
// const n = 1048576;    // number of items to sort
// const n = 524288;     // number of items to sort
// const n = 131072;     // number of items to sort
// const n = 20000;      // number of items to sort
// const n = 2000;       // number of items to sort
// const n = 15;            // number of items to sort
// const nFac = 1307674368000;  // factorial of n
// const n = 14;            // number of items to sort
// const nFac = 87178291200;  // factorial of n
// const n = 13;            // number of items to sort
// const nFac = 6227020800;  // factorial of n
const n = 12;            // number of items to sort
const nFac = 479001600;  // factorial of n
// const n = 11;            // number of items to sort
// const nFac = 39916800;   // factorial of n
// const n = 10;           // number of items to sort
// const nFac = 3628800;   // factorial of n
// const n = 9;           // number of items to sort
// const nFac = 362880;   // factorial of n
// const n = 8;          // number of items to sort
// const nFac = 40320;   // factorial of n
// const n = 7;         // number of items to sort
// const nFac = 5040;   // factorial of n
// const n = 6;        // number of items to sort
// const nFac = 720;   // factorial of n
// const n = 5;        // number of items to sort
// const nFac = 120;   // factorial of n
// const n = 4;       // number of items to sort
// const nFac = 24;   // factorial of n
// const n = 3;      // number of items to sort
// const nFac = 6;   // factorial of n
// const n = 2;      // number of items to sort
// const nFac = 2;   // factorial of n
let a = Array(n);
a.fill(1);
a = a.map(x => Math.ceil(Math.random() * n * 10));
// a = a.map(x => Math.ceil(Math.random() * n / 100));  // just for testing out the counting sort
// let a = [ 121, 40, 71, 8, 111, 400, 191, 4, 11 ];  // testing effect of duplicates on the bogus sort
const mainNum = Math.floor(Math.random() * (a.length - 10));
let mainRes = '';
const statTracking = [];
mainRes += `\nPart of Original Unsorted Array.\nSample(${mainNum}, ${mainNum + 9}): ${stringUpAnArray(a.slice(mainNum, mainNum + 10))}\n`;

/**
 * Give me an array and two indexes and I will swap them and return control to you
 * this swap is type independent - it does not care what the type is of the array at those positions
 * @param {*} arr - the original array
 * @param {*} index1 - the first index
 * @param {*} index2 - the second index
 */
function inPlaceSwapper (arr, index1, index2) {
    let tempValue = arr[index1];
    arr[index1] = arr[index2];
    arr[index2] = tempValue;
}

/**
 * make my own sliced string to print out sections of arrays because I prefer:
 * [ ..., 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...]
 * over:
 * 1,2,3,4,5,6,7,8,9,10
 * @param {*} array
 */
function stringUpAnArray (array) {
    let str = '[ ..., ';
    str += array.join(', ')
    return str + ', ... ]';
}

function isSorted (arr) {
    for (let i in arr) {
        if (arr[i] > arr[+i + 1]) {
            return false;
        }
    }
    return true;
}

const sum = (a, c) => a + c;

/**
 * Given a sorted array of integers
 * @param {Array} arr
 */
function findMedian (arr) {
    let median = -2;
    let middle = -2;
    let sorted = [...arr].sort((a, b) => a - b);
    if ((arr.length % 2) == 0) {
        // even case
        middle = arr.length / 2;
        // median = (arr[middle] + arr[middle + 1] / 2);
        median = ((sorted[middle] + sorted[middle - 1]) / 2);
    } else {
        // odd case
        middle = (arr.length + 1) / 2;
        // median = arr[middle];
        median = sorted[middle - 1];
    }
    // console.log(`Finding the median and this is what we have:
    //      Array  [ ${arr} ]
    //      Sorted [ ${sorted} ]
    //      Length  ${arr.length}
    //      Middle  ${middle}
    //      Median  ${median}\n`);
    return median;
}

const sorts = [
    // function insertion (a) {
    //     const insertionStartTime = Date.now();
    //     console.log(`\nInsertion Sort for ${n} items.`);

    //     let temp = -1;
    //     const len = a.length;
    //     for (let i = 1; i < len; i++) {
    //         for (let k = i; k > 0; k--) {
    //             temp = a[k - 1];
    //             if (temp > a[k]) {
    //                 a[k - 1] = a[k];
    //                 a[k] = temp;
    //             } else {
    //                 break;
    //             }
    //         }
    //     }

    //     let insertionNum = Math.floor(Math.random() * (a.length - 10));
    //     // console.log(a.slice(num, num + 10));
    //     const insertionEndTime = Date.now();
    //     let res = `\nInsertion sort has taken ${insertionEndTime - insertionStartTime}ms to complete.\nSample(${insertionNum}, ${insertionNum + 9}): ${stringUpAnArray(a.slice(insertionNum, insertionNum + 10))}\n`;
    //     return res;
    // },
    // function selection (b) {
    //     const selectionStartTime = Date.now();
    //     console.log(`Selection Sort for ${n} items.`);
    //     // console.log(`\nPart of Original Unsorted Array.\n${a.slice(mainNum, mainNum + 10)}\n`);

    //     let newArr = [];

    //     // we start with an array
    //     // we make an empty array
    //     // we start going through the original array to find the smallest number
    //     // we place that number in the front of the new array
    //     // we do that over and over until there are no elements left in the original array
    //     // tada the new array is sorted
    //     while (b.length > 0) {
    //         let smallestIndex = -1;
    //         let smallestValue = Number.MAX_VALUE;
    //         for (let i = 0; i < b.length; i++) {
    //             if (b[i] < smallestValue) {
    //                 smallestIndex = i;
    //                 smallestValue = b[i];
    //             }
    //         }
    //         newArr.push(smallestValue);
    //         b.splice(smallestIndex, 1);
    //         // console.log(newArr);
    //         // console.log(b);
    //     }

    //     let selectionNum = Math.floor(Math.random() * (a.length - 10));
    //     const selectionEndTime = Date.now();
    //     let res = `\nSelection sort has taken ${selectionEndTime - selectionStartTime}ms to complete.\nSample(${selectionNum}, ${selectionNum + 9}): ${stringUpAnArray(newArr.slice(selectionNum, selectionNum + 10))}\n`;
    //     return res;
    // },
    // function merge (c) {
    //     const mergeStartTime = Date.now();
    //     console.log(`Merge     Sort for ${n} items.`);

    //     let newArr = [];
    //     function divide (c1) {
    //         if (c1.length > 1) {
    //             let half = Math.floor(c1.length / 2);
    //             // console.log(c1);
    //             return conquer(divide(c1.slice(0, half)), divide(c1.slice(half)));
    //         }
    //         else return c1;
    //     }
    //     function conquer (c2, c3) {
    //         let c4 = [];
    //         let in2 = 0, in3 = 0;
    //         while (in2 < c2.length && in3 < c3.length) {
    //             if (c2[in2] < c3[in3]) {
    //                 c4.push(c2[in2]);
    //                 in2++;
    //             } else {
    //                 c4.push(c3[in3]);
    //                 in3++;
    //             }
    //         }
    //         if (in2 >= c2.length) {
    //             // c4.splice(c4.length, 0, c3.slice(in3));
    //             for (let i = in3; i < c3.length; i++) {
    //                 c4.push(c3[i]);
    //             }
    //         } else {
    //             // c4.splice(c4.length, 0, c2.slice(in2));
    //             for (let i = in2; i < c2.length; i++) {
    //                 c4.push(c2[i]);
    //             }
    //         }
    //         // const assert = require('assert');
    //         // console.log(c2.length, c3.length, c4.length);
    //         // assert.equal(c4.length, c2.length + c3.length);
    //         return c4;
    //     }
    //     newArr = divide(c);

    //     let mergeNum = Math.floor(Math.random() * (a.length - 10));
    //     const mergeEndTime = Date.now();
    //     // console.log(newArr.length);
    //     let res = `\nMerge sort has taken ${mergeEndTime - mergeStartTime}ms to complete.\nSample(${mergeNum}, ${mergeNum + 9}): ${stringUpAnArray(newArr.slice(mergeNum, mergeNum + 10))}\n`;
    //     // console.log(a);
    //     console.log(res);
    //     return res;
    // },
    // function heap (d) {
    //     const heapStartTime = Date.now();
    //     console.log(`Heap      Sort for ${n} items.`);
    //     const parent = x => Math.ceil(x/2) - 1;
    //     const leftChild = x => x * 2 + 1;
    //     const rightChild = x => x * 2 + 2;

    //     function heapify (d1) {
    //         for (let i = parent(d1.length - 1); i > 0; i--) {   // I don't think the = is necessary, but I've been wrong before
    //         // for (let i = parent(d1.length - 1); i >= 0; i--) {
    //             siftDown(d1, i, d1.length - 1);
    //         }
    //         return d1;
    //     }

    //     function siftDown (d2, start, end) {
    //         let root = start;
    //         while (leftChild(root) <= end) {
    //             let child = leftChild(root);
    //             let swap = root;

    //             if (d2[swap] < d2[child]) {
    //                 swap = child;
    //             }
    //             if (child + 1  <= end && d2[swap] < d2[child+1]) {
    //                 swap = child + 1;
    //             }
    //             if (swap == root) return;
    //             else {
    //                 // let temp = d2[root];
    //                 // d2[root] = d2[swap];
    //                 // d2[swap] = temp;
    //                 inPlaceSwapper(d2, root, swap);
    //                 root = swap;
    //             }
    //         }
    //         return;
    //     }

    //     const heap = heapify(d);
    //     // let trackingIndex = d.length - 1;
    //     for (let end = heap.length - 1; end > 0; end--) {
    //         siftDown(heap, 0, end);
    //         // swap
    //         inPlaceSwapper(heap, 0, end);
    //         // let temp = heap[0];
    //         // heap[0] = heap[end];
    //         // heap[end] = temp;
    //     }

    //     let heapNum = Math.floor(Math.random() * (a.length - 10));
    //     const heapEndTime = Date.now();
    //     let res = `\nHeap sort has taken ${heapEndTime - heapStartTime}ms to complete.\nSample(${heapNum}, ${heapNum + 9}): ${stringUpAnArray(heap.slice(heapNum, heapNum + 10))}\n`;
    //     // console.log(parent(128));
    //     return res;
    // },
    // function quick (e) {
    //     const quickStartTime = Date.now();
    //     console.log(`Quick     Sort for ${n} items.`);

    //     function quicksort (e1, lo, hi) {
    //         if (lo < hi) {
    //             let p = partition(e1, lo, hi);
    //             quicksort(e1, lo, p - 1);
    //             quicksort(e1, p + 1, hi);
    //         }
    //     }

    //     function partition (e2, lo, hi) {
    //         let mid = Math.floor((lo + hi) / 2);
    //         let temp;
    //         if (e2[mid] < e2[lo]) {
    //             inPlaceSwapper(e2, mid, lo);
    //             // temp = e2[mid];
    //             // e2[mid] = e2[lo];
    //             // e2[lo] = temp;
    //         }
    //         if (e2[hi] < e2[lo]) {
    //             inPlaceSwapper(e2, lo, hi);
    //             // temp = e2[hi];
    //             // e2[hi] = e2[lo];
    //             // e2[lo] = temp;
    //         }
    //         if (e2[mid] < e2[hi]) {
    //             inPlaceSwapper(e2, mid, hi);
    //             // temp = e2[mid];
    //             // e2[mid] = e2[hi];
    //             // e2[hi] = temp;
    //         }
    //         let pivot = e2[hi];
    //         let i = lo;
    //         for (let k = lo; k < hi; k++) {
    //             if (e2[k] < pivot) {
    //                 if (i != k) {
    //                     inPlaceSwapper(e2, i, k);
    //                     // temp = e2[k];
    //                     // e2[k] = e2[i];
    //                     // e2[i] = temp;
    //                 }
    //                 i++;
    //             }
    //         }
    //         inPlaceSwapper(e2, hi, i);
    //         // temp = e2[hi];
    //         // e2[hi] = e2[i];
    //         // e2[i] = temp;
    //         return i;
    //     }
    //     quicksort(e, 0, e.length - 1);

    //     let quickNum = Math.floor(Math.random() * (a.length - 10));
    //     const quickEndTime = Date.now();
    //     let res = `\nQuick sort has taken ${quickEndTime - quickStartTime}ms to complete.\nSample(${quickNum}, ${quickNum + 9}): ${stringUpAnArray(e.slice(quickNum, quickNum + 10))}\n`;
    //     // console.log(a);
    //     return res;
    // },
    // function shell (f) {
    //     const shellStartTime = Date.now();
    //     console.log(`Shell     Sort for ${n} items.`);

    //     let gaps = [701, 301, 132, 57, 23, 10, 4, 1];
    //     // let gaps = [23, 10, 4, 1];   // for experimentation
    //     for (let gap of gaps) {
    //         // console.log('Running', gap);
    //         for (let i = gap; i < f.length; i++) {
    //             let temp  = f[i];
    //             let j = i;
    //             for (; j >= gap && f[j - gap] > temp; j -= gap) {
    //                 f[j] = f[j - gap];
    //             }
    //             f[j] = temp;
    //         }
    //     }

    //     let shellNum = Math.floor(Math.random() * (a.length - 10));
    //     const shellEndTime = Date.now();
    //     let res = `\nShell sort has taken ${shellEndTime - shellStartTime}ms to complete.\nSample(${shellNum}, ${shellNum + 9}): ${stringUpAnArray(f.slice(shellNum, shellNum + 10))}\n`;
    //     // console.log(a);
    //     return res;
    // },
    // function bubble (g) {
    //     const bubbleStartTime = Date.now();
    //     console.log(`Bubble    Sort for ${n} items.`);

    //     // with shortening the length optimization
    //     /* let swaps = 1;
    //     let len = g.length;
    //     while (swaps > 0) {
    //         swaps = 0;
    //         for  (let i = 1; i < len; i ++) {
    //             if (g[i - 1] > g[i]) {
    //                 swaps++;
    //                 let temp = g[i];
    //                 g[i] = g[i - 1];
    //                 g[i - 1] = temp;
    //             }
    //         }
    //         len--;
    //     } */

    //     // with shortening length to where last swap occurred
    //     let len = g.length;
    //     let newn = 10;
    //     while (newn > 1) {
    //         newn = 0;
    //         for  (let i = 1; i < len; i ++) {
    //             if (g[i - 1] > g[i]) {
    //                 newn = i;
    //                 let temp = g[i];
    //                 g[i] = g[i - 1];
    //                 g[i - 1] = temp;
    //             }
    //         }
    //         len = newn;
    //     }

    //     let bubbleNum = Math.floor(Math.random() * (g.length - 10));
    //     const bubbleEndTime = Date.now();
    //     let res = `\nBubble sort has taken ${bubbleEndTime - bubbleStartTime}ms to complete.\nSample(${bubbleNum}, ${bubbleNum + 9}): ${stringUpAnArray(g.slice(bubbleNum, bubbleNum + 10))}\n`;
    //     // console.log(a);
    //     return res;
    // },
    // function comb (h) {
    //     const combStartTime = Date.now();
    //     console.log(`Comb      Sort for ${n} items.`);

    //     let gap = h.length;
    //     const shrink = 1.3;
    //     let sorted = false;
    //     while (!sorted) {
    //         gap = Math.floor(gap / shrink);
    //         if (gap <= 1) {
    //             gap = 1;
    //             sorted = true;
    //         }

    //         for (let i = 0; (i + gap) < h.length; i++) {
    //             if (h[i] > h[i + gap]) {
    //                 sorted = false;
    //                 inPlaceSwapper(h, i, i + gap);
    //                 // let temp = h[i + gap];
    //                 // h[i + gap] = h[i];
    //                 // h[i] = temp;
    //             }
    //         }
    //     }

    //     let combNum = Math.floor(Math.random() * (h.length - 10));
    //     const combEndTime = Date.now();
    //     let res = `\nComb sort has taken ${combEndTime - combStartTime}ms to complete.\nSample(${combNum}, ${combNum + 9}): ${stringUpAnArray(h.slice(combNum, combNum + 10))}\n`;
    //     // console.log(a);
    //     return res;
    // },
    // function counting (i) {
    //     // the RANGE of the items you are sorting also has a great effect on the efficiency of the counting sort
    //     const countingStartTime = Date.now();
    //     console.log(`Counting  Sort for ${n} items.`);

    //     // let i1 = Array(Math.ceil(n / 100)).fill(0);   // to reduce the RANGE
    //     let i1 = Array((n * 10) + 1).fill(0);   //count ← new array of k zeros

    //     // for i = 1 to length(array) do
    //     //     count[array[i]] ← count[array[i]] + 1
    //     i.map((currentValue, index) => i1[currentValue]++);

    //     // for i = 2 to k do
    //     //     count[i] ← count[i] + count[i - 1]
    //     i1.map((currentValue, index) => {if (index != 0) i1[index] += i1[index - 1]});

    //     // for i = length(array) downto 1 do
    //     //     output[count[array[i]]] ← array[i]
    //     //     count[array[i]] ← count[array[i]] - 1
    //     let i2 = Array(n);
    //     for (let j = i.length - 1; j >= 0; j--) {
    //         i2[i1[i[j]] - 1] = i[j];
    //         i1[i[j]]--;
    //     }

    //     let countingNum = Math.floor(Math.random() * (i.length - 10));
    //     const countingEndTime = Date.now();
    //     let res = `\nCounting sort has taken ${countingEndTime - countingStartTime}ms to complete.\nSample(${countingNum}, ${countingNum + 9}): ${stringUpAnArray(i2.slice(countingNum, countingNum + 10))}\n`;
    //     // console.log(a);
    //     return res;
    // },
    // function bucket (j) {
    //     const bucketStartTime = Date.now();
    //     console.log(`Bucket    Sort for ${n} items.`);

    //     let numBuckets = Math.floor(j.length / 10);
    //     let buckets = Array(numBuckets).fill([]);
    //     let maxValue = j.reduce((max, cur) => Math.max(max, cur));
    //     console.log(buckets[Math.floor(j[0] / maxValue * numBuckets)], numBuckets);
    //     // j.map(currentValue => console.log(currentValue));
    //     j.map(currentValue => buckets[2 - 2].push(currentValue));
    //     // j.map(currentValue => buckets[Math.floor(currentValue / maxValue * numBuckets)].push(currentValue));
    //     console.log(buckets);

    //     let bucketNum = Math.floor(Math.random() * (j.length - 10));
    //     const bucketEndTime = Date.now();
    //     let res = `\nBucket sort has taken ${bucketEndTime - bucketStartTime}ms to complete.\nSample(${bucketNum}, ${bucketNum + 9}): ${stringUpAnArray(j.slice(bucketNum, bucketNum + 10))}\n`;
    //     // console.log(a);
    //     return res;
    // },
    // function radix (k) {
    //     const radixStartTime = Date.now();
    //     console.log(`Radix     Sort for ${n} items.`);



    //     let radixNum = Math.floor(Math.random() * (k.length - 10));
    //     const radixEndTime = Date.now();
    //     let res = `\nRadix sort has taken ${radixEndTime - radixStartTime}ms to complete.\nSample(${radixNum}, ${radixNum + 9}): ${stringUpAnArray(k.slice(radixNum, radixNum + 10))}\n`;
    //     // console.log(a);
    //     return res;
    // },
    // function tim (l) {
    //     console.log(`Tim Sort for ${n} items.`);
    //     let res = '';
    //     // console.log(a);
    //     return res;
    // },
    // function library (m) {
    //     console.log(`Library Sort for ${n} items.`);
    //     let res = '';
    //     // console.log(a);
    //     return res;
    // },
    // function slow (n) {
    //     const slowStartTime = Date.now();
    //     console.log(`Slow      Sort for ${n.length} items.`);
    //     let res = '';

    //     function slowdown (n1, i, k) {
    //         if (i >= k) return;
    //         let middle = Math.floor((i + k) / 2);
    //         slowdown(n1, i, middle);
    //         slowdown(n1, middle + 1, k);
    //         if (n1[k] < n1[middle]) {
    //             inPlaceSwapper(n1, k, middle);
    //         }
    //         slowdown(n1, i, k - 1);
    //     }
    //     slowdown(n, 0, n.length - 1);

    //     let slowNum = Math.floor(Math.random() * (n.length - 10));
    //     const slowEndTime = Date.now();
    //     res = `\nSlow Sort has taken ${slowEndTime - slowStartTime}ms to complete.\nSample(${slowNum}, ${slowNum + 9}): ${stringUpAnArray(n.slice(slowNum, slowNum + 10))}\n`;;
    //     // console.log(n);
    //     return res;
    // },
    // function stooge (o) {
    //     const stoogeStartTime = Date.now();
    //     console.log(`Stooge    Sort for ${n} items.`);
    //     let res = '';

    //     // function stoogesort(array L, i = 0, j = length(L)-1){
    //     function stoogesort (o1, i, k) {
    //         if (o1[i] > o1[k]) {
    //             inPlaceSwapper(o1, i, k);
    //         }
    //         if ((k - (i + 1)) > 2) {
    //             let t = Math.round((k - (i + 1)) / 3);
    //             stoogesort(o1, i, k - t);
    //             stoogesort(o1, i + t, k);
    //             stoogesort(o1, i, k - t);
    //         }
    //     }
    //     stoogesort(o, 0, o.length - 1);
    //     //     if L[i] > L[j] then
    //     //         L[i] ↔ L[j]
    //     //     if (j - i + 1) > 2 then
    //     //         t = (j - i + 1) / 3
    //     //         stoogesort(L, i  , j-t)
    //     //         stoogesort(L, i+t, j  )
    //     //         stoogesort(L, i  , j-t)
    //     //     return L
    //     // }

    //     let stoogeNum = Math.floor(Math.random() * (o.length - 10));
    //     const stoogeEndTime = Date.now();
    //     res = `\nStooge Sort has taken ${stoogeEndTime - stoogeStartTime}ms to complete.\nSample(${stoogeNum}, ${stoogeNum + 9}): ${stringUpAnArray(o.slice(stoogeNum, stoogeNum + 10))}\n`;;
    //     // console.log(o);
    //     return res;
    // },
    function bogo (p) {
        const bogoStartTime = Date.now();
        console.log(`Bogo      Sort for ${n} items.`);
        let res = '';

        let count = 0;
        while (!isSorted(p)) {
            count++;
            let p1 = [...p];
            for (let i in p) {
                let magicNum = Math.floor(Math.random() * p1.length);
                p[i] = p1[magicNum];
                p1.splice(magicNum, 1);
                // console.log(p1);
            }
            // console.count('bogo');
            if (count % 1000000 === 0)
                // console.log(`${p} \t ${Math.floor(count/1000000)} *1M tries and counting @ ${new Date().toLocaleTimeString()}.`);
                console.log(`${p} \t ${Math.floor(count/1000000)} *1M tries and counting @ ${Date.now()}.`);
        }

        // const nFac = 3628800;   // 10 factorial
        // const nFac = 5040;   // 7 factorial
        // console.log(count, count / nFac);
        let bogoNum = Math.floor(Math.random() * (p.length - 10));
        const bogoEndTime = Date.now();
        const bogoDuration = bogoEndTime - bogoStartTime;
        statTracking.push({permutations: count, sortTime: bogoDuration});
        res = `\nBogo Sort has taken ${bogoDuration}ms to complete.\nSample(${bogoNum}, ${bogoNum + 9}): ${stringUpAnArray(p.slice(bogoNum, bogoNum + 10))}\n`;;
        console.log(p);
        return res;
    }
];

// for (s of sorts) {
//     mainRes += s([ ... a]);
// }
// console.log(mainRes);
const iterations = 1;
for (let i = 0; i < iterations; i++) {
    mainRes += sorts[0]([ ... a ]);
}

const mainEndTime = Date.now();
const ms = mainEndTime - mainStartTime;
// console.log(statTracking[19], statTracking[19].sortTime, statTracking[19].permutations);

const min = Math.min(statTracking.map((y, z) => statTracking[z].sortTime));
const max = Math.max(statTracking.map((y, z) => statTracking[z].sortTime));
let one = statTracking.map((w, x) => statTracking[x].permutations);
let two = statTracking.map((y, z) => statTracking[z].sortTime);
const minOps = Math.min(...one);
const maxOps = Math.max(...one);
const minTimes = Math.min(...two);
const maxTimes = Math.max(...two);
// console.log(one, two, one.length, two.length);
// console.log('Median: ', findMedian(one.sort()), '\t\tMean: ', one.reduce(sum) / iterations);
// console.log('Median: ', findMedian(two.sort()), 'ms\t\tMean: ', two.reduce(sum) / iterations, 'ms');

/**
 *
 * @param {*} arr
 */
function findMode (arr) {
    let modeArr = {};
    let res = [];
    for (let i in arr) {
        if (modeArr[arr[i]]) {
            modeArr[arr[i]]++;
        } else {
            modeArr[arr[i]] = 1;
        }
    };
    let mode = Math.max(...Object.values(modeArr));
    // console.log(mode);
    for (let tries in modeArr) {
        if (modeArr[tries] == mode) {
            res.push(tries);
        }
    }
    return `Mode(s):  ${formatThis(res)} which ocurred  ${mode} times`;

    function formatThis (numArr) {
        let newArr = numArr.map(num => num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'));
        newStr = newArr.join(',  ');
        return newStr;
    }
}
// console.log(two);
console.log(`this sort testing program has taken ${mainEndTime - mainStartTime}ms to complete.`);
const permutationMean = one.reduce(sum) / iterations;
console.log(`\nBogus Sort:\n
            run  ${iterations.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} times on ${n} integers
            taking  ${(mainEndTime - mainStartTime).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} ms to complete,
            or  ${Math.floor(ms/60000)} minutes  ${Math.floor(ms/1000%60)} seconds  and ${ms%1000} ms.

            An average overall runtime of ${Math.floor(ms/iterations).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}${(Math.round(((ms/iterations) - Math.floor(ms/iterations))*10000)/10000).toString().substring(1)} ms per sort (or  ${Math.floor(ms/iterations/60000)} minutes  ${Math.floor(ms/iterations/1000%60)} seconds  and ${ms/iterations%1000} ms),
            and an average overall of ${Math.round(((ms/iterations)/n)*10000)/10000} ms per item (or  ${Math.floor(ms/iterations/n/60000)} minutes  ${Math.floor(ms/iterations/n/1000%60)} seconds  and ${Math.round((ms/iterations/n%1000)*10000)/10000} ms).

            Bogus Sort has a complexity of n! (n factorial)
            with  ${n} items expect  ${n}! =  ${(nFac).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}

            Permutations:
            Median:   ${findMedian(one).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}\t\tMean:  ${Math.floor(permutationMean).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}${(Math.round((permutationMean - Math.floor(permutationMean))*10000)/10000).toString().substring(1)} (${Math.round((permutationMean / nFac)*10000)/10000} times the expected n!)
            ${findMode(one)}
            Minimum:  ${minOps.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} (about 1/${Math.round(1/(minOps/nFac))}th the expected n!)\tMaximum  ${maxOps.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} (${Math.round((maxOps/nFac)*10000)/10000} times the expected n!)\n
            Sort Time:
            Median:   ${findMedian(two).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} ms\t\tMean:  ${Math.floor(two.reduce(sum) / iterations).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}${(Math.round(((two.reduce(sum)/iterations) - Math.floor(two.reduce(sum)/iterations))*10000)/10000).toString().substring(1)} ms
            ${findMode(two)}
            Minimum:  ${minTimes.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} ms\tMaximum  ${maxTimes.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')} ms\n
            `);
console.log(new Date().toLocaleString());
console.timeEnd('Sorting');
console.log(process.uptime());

// The following is Progress Bar code that I found as an aside, yet placed here to try it out
// const ProgressBar = require('progress')

// const bar = new ProgressBar(':bar', { total: 80 })
// const timer = setInterval(() => {
//   bar.tick()
//   if (bar.complete) {
//     clearInterval(timer)
//   }
// }, 100)
